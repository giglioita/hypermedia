# HYPERMEDIA - BIG GYM PROJECT
## Group members
 * Nicola Dellarocca
 * Francesco Giglioli
 * Alessandro Tribi

## What we used:
 * Bootstrap (version 3.3.4)
 * AngularJS (version 1.3.15)
 * jQuery (version 1.11.3)

## Mobile version
In order to create the mobile version of the app (with Phonegap Build) one only needs to change the following line:
`<base href="/hypermedia/WEBSITE/" />` to `<base href="/android_asset/www/" />` (android only, for iOS the href is something like `/var/mobile/Applications/<app_code>/<app_name>.app/www`).

## Known bugs:
 * In Firefox there is a bug known as "pixel snapping" that makes HTML elements with 'transform' CSS property set be unaligned respect to the container ([link](https://bugzilla.mozilla.org/show_bug.cgi?id=739176))
 * The height of the Google Map must be set before initializing it, otherwise some tiles won't load ([link](http://gis.stackexchange.com/questions/32290/google-maps-openlayers-only-rendering-top-left-tiles))
 * There is a bug in Chrome and Firefox (actually there are 2 different bugs, [one on Chrome](https://code.google.com/p/google-cast-sdk/issues/detail?id=309) and [one on Firefox](https://bugzilla.mozilla.org/show_bug.cgi?id=1127577)) that makes the browser print a stack trace in the console whenever a YouTube video is embedded in the page.
 * Chrome Canary and Safari log an error because YouTube videos have an HTTPS protocol, while our website has HTTP.
 * On low-resolution screens the dropdown arrows appear blurred