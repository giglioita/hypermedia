<?php
define('FACEBOOK_SDK_V4_SRC_DIR', 'facebook/src/Facebook/');
require 'functions/Model.php';
require __DIR__ . '/facebook/autoload.php';

use Facebook\FacebookSession;
use Facebook\FacebookRequest;

$db = new DB();
Utility::init();

// ID
if (isset($_GET['id'])) {
	$id = Utility::toInt(Utility::getParam('id'));
	if ($id != null) {
		Utility::addParam('id', $id, PDO::PARAM_INT);
	}
}

$query = 'SELECT FacebookID FROM istruttori WHERE ID = :id';
if (Utility::hasParameters()) {
	/* Get all instructors */
	$instructors = $db->query($query, Utility::getParamNames(), Utility::getParamValues(), Utility::getParamTypes());

	if (!$instructors) {
		// array is empty
		Utility::fail("Failed retrieving photos from instructor with id $id", 500);
		die();
	}
	
	$facebookID = $instructors[0]->FacebookID;
	
} else {
	Utility::fail('No parameter specified', 400);
	die();
}

$db = null;
Utility::finish();

$content = file_get_contents('fb_access_token.txt');
$secret = preg_split('/\s/', $content);

FacebookSession::setDefaultApplication($secret[0], $secret[1]);
$session = FacebookSession::newAppSession();

$request = new FacebookRequest($session, 'GET', "/$facebookID/photos");
$response = $request->execute();
$graphObject = $response->getGraphObject()->asArray();


$result = array();
for ($i=0; $i<3; $i++) {

	$obj = new stdClass();

	$imagesArray = $graphObject['data'][$i]->images;
	$length = count($imagesArray);

	if ($length > 0) {
		$obj->original = $imagesArray[0]->source;
		$obj->small = $imagesArray[$length-1]->source;
	}
	
	array_push($result, $obj);
}

echo json_encode($result);
?>