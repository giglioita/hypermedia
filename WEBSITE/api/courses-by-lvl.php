<?php
require 'functions/Model.php';

$db = new DB();

/* Get all categories */
$query = 'SELECT ID, Nome FROM categorie';

$categories = $db->query($query);

if (!$categories) {
	Utility::fail('Error retrieving categories from database', 500);
    die();
}

/* Get all courses by level */
$query = 'SELECT ID, Nome, Livello, Categoria, AddressedTo FROM corsi ORDER BY Categoria, Livello, Nome';

$courses = $db->query($query);

if (!$courses) {
	Utility::fail('Error retrieving courses from database', 500);
} else {
    /* Assign to each category its courses, divided into three groups: beginner, intermediate and advanced */
    foreach ($categories as $category) {
        $course = reset($courses);
        while ($course->Categoria != $category->ID) {
            $course = next($courses);
        }

        $begCourses = array();
        while ($course != null && $course->Categoria == $category->ID && $course->Livello == 1) {
            $begCourses[] = $course;
            $course = next($courses);
        }
        $category->corsiBeginner = $begCourses;

        $intCourses = array();
        while ($course != null && $course->Categoria == $category->ID && $course->Livello == 2) {
            $intCourses[] = $course;
            $course = next($courses);
        }
        $category->corsiIntermediate = $intCourses;

        $advCourses = array();
        while ($course != null && $course->Categoria == $category->ID && $course->Livello == 3) {
            $advCourses[] = $course;
            $course = next($courses);
        }
        $category->corsiAdvanced = $advCourses;
    }

	echo json_encode($categories);
}

$db = null;
?>
