<?php
require 'functions/Model.php';

$db = new DB();

/*Get all courses A-Z*/
$query = 'SELECT CA.Nome AS \'NomeCategoria\', CO.ID AS \'ID\', CO.Nome AS \'NomeCorso\', CO.Livello AS \'LivelloCorso\', CO.AddressedTo AS \'AddressedTo\' FROM categorie CA JOIN corsi CO ON CA.ID = CO.Categoria ORDER BY CO.Nome';

$courses = $db->query($query);

if (!$courses) {
	Utility::fail('Error retrieving courses from database', 500);
} else {
	echo json_encode($courses);
}

$db = null;
?>
