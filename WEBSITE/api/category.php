<?php
require 'functions/Model.php';

$db = new DB();
Utility::init();

// Add param ID if present
if (isset($_GET['id'])) {
	$id = Utility::toInt(Utility::getParam('id'));
	if ($id != null) {
		Utility::addParam('id', $id, PDO::PARAM_INT);
	}
}

/* Get category with ID = id */
$query = 'SELECT * FROM categorie WHERE ID = :id';

if (Utility::hasParameters()) {
	$category = $db->query($query, Utility::getParamNames(), Utility::getParamValues(), Utility::getParamTypes());
	
	if (count($category) > 0) {
		$category = $category[0];
	} else {
		Utility::fail("Failed to retrieve category with id $id", 500);
	}

	/* Get all teachers in category */
	$query = '	SELECT IST.ID, IST.Nome, IST.Cognome, IST.Foto, IST.Bio
				FROM (teaches_2 T2 JOIN categorie CA ON T2.Categoria = CA.ID)
					JOIN istruttori IST ON IST.ID = T2.Istruttore
				WHERE CA.ID = :id
				ORDER BY IST.Cognome, IST.Nome';
	$instructors = $db->query($query, Utility::getParamNames(), Utility::getParamValues(), Utility::getParamTypes());
	$category->instructors = $instructors;
	
	/* Get all courses offered in this category */
	$query = '	SELECT C.ID, C.Nome, C.AddressedTo, L.Nome as \'Livello\'
				FROM corsi C JOIN livelli L ON C.Livello = L.ID
				WHERE C.Categoria = :id
				ORDER BY C.Nome, L.ID';
	$courses = $db->query($query, Utility::getParamNames(), Utility::getParamValues(), Utility::getParamTypes());
	$category->courses = $courses;

	// error handling
	if (!$category) {
		Utility::fail("Failed to retrieve category with id $id", 500);
	} else {
		echo json_encode($category);
	}
	
} else {
	Utility::fail('No parameter specified', 400);
}

$db = null;
Utility::finish();
?>