<?php
require 'functions/Model.php';

$db = new DB();

/* Get data categories */
$query = 'SELECT ID, Nome, Copertina, intro, Slogan FROM categorie ORDER BY Nome;';
$categorie = $db->query($query);

/* Error handling */
if (!$categorie) {
	Utility::fail('Failed to retrieve categories from database', 500);
} else {
	echo json_encode($categorie);
}

$db = null;
?>