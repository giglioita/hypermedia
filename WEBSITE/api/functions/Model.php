<?php
require_once 'Constants.php'; /* For using constants */
require_once 'Utility.php'; /* Utility functions */

/* response will be in json */
header('Content-Type: application/json; charset=utf-8');
header('Access-Control-Allow-Origin: *');

class DB {
	
	private $debug;
	private $db;
	
	private static $SUPPORTED_TYPES = array(PDO::PARAM_BOOL, PDO::PARAM_NULL, PDO::PARAM_INT, PDO::PARAM_STR);
	
	public function __construct($debug = false) {
		$this->debug = $debug;
		$this->db = new PDO('mysql:host=127.0.0.1;dbname=my_friendsfindermwd', 'friendsfindermwd', 'hypermedia');
		if (!$this->db) {
			Utility::fail('Error connecting DB', 500, Constants::ERR_DB_CONNECTION);
		}
		
		$this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
	}
	
	public function query($query, $params = null, $paramValues = null, $paramTypes = null) {
		
		/* If not connected to db do nothing */
		if (!$this->db) {
			return;
		}
		$size = count($params);
		if ($size !== count($paramTypes) || $size !== count($paramValues)) {
			Utility::fail('Array must have same length', 500);
		}
		
		$prepared = $this->db->prepare($query);
		
		if (!$prepared) {
			Utility::fail('Query preparation failed', 500);
		}
		
		for ($i=0; $i<$size; $i++) {
			if (!in_array($paramTypes[$i], self::$SUPPORTED_TYPES, true)) {
				Utility::fail("Type $paramTypes[$i] not supported", 500);
			}

			$bound = $prepared->bindParam($params[$i], $paramValues[$i]);
			
			if (!$bound) {
				Utility::fail("Error when binding param $params[$i]", 500);
			}
		}
		
		if ($this->debug) {
			echo '<pre>';
			$prepared->debugDumpParams();
			echo '</pre>';
		}
		
		$executed = $prepared->execute();
		if (!$executed) {
			Utility::fail("Error executing query: $db->errorCode(); $db->errorInfo()", 500);
			/* If there is an error executing the query, everything should stop */
			die();
		}
		
		$result = $prepared->fetchAll(PDO::FETCH_OBJ);
		if ($this->debug) {
			echo print_r($result);
		}
		return $result;
	}
	
	public function __destruct() {
		// disconnect from DB
		$this->db = null;
	}
	
}
?>