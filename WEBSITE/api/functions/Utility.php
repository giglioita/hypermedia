<?php
/* Utility functions */
class Utility {
	
	private static $paramNames, $paramTypes, $paramValues;
	private static $isInit = false;
	
	/* Initialize Utility */
	public static function init() {
		self::$paramNames = array();
		self::$paramTypes = array();
		self::$paramValues = array();
		self::$isInit = true;
	}
	
	/* Obtain params from $_GET */
	public static function getParam($name) {
		if (isset($_GET[$name])) {
			return htmlentities($_GET[$name]);
		}
		return null;
	}

	/* Parse $value as an int, otherwise return null */
	public static function toInt($value) {
		return ctype_digit($value) ? intval($value) : null;
	}

	/* Add param to Utility's internal array */
	public static function addParam($name, $value, $type = PDO::PARAM_STR) {
		if (!Utility::$isInit) {
			throw new Exception('Utility must be initialized');
		}
		array_push(Utility::$paramNames, $name);
		array_push(Utility::$paramTypes, $type);
		array_push(Utility::$paramValues, $value);
	}
	
	/* Returns true only if there is at least one parameter (name, value and type) */
	public static function hasParameters() {
		$count = count(Utility::$paramNames);
		if ($count > 0) {
			return $count === count(Utility::$paramValues) && $count === count(Utility::$paramTypes);
		} else {
			return false;
		}
	}
	
	/* Getter for paramNames */
	public static function getParamNames() {
		if (!self::$isInit) {
			throw new Exception('Utility is not initialized');
		}
		
		return self::$paramNames;
	}
	
	/* Getter for paramValues */
	public static function getParamValues() {
		if (!self::$isInit) {
			throw new Exception('Utility is not initialized');
		}
		
		return self::$paramValues;
	}
	
	/* Getter for paramTypes */
	public static function getParamTypes() {
		if (!self::$isInit) {
			throw new Exception('Utility is not initialized');
		}
		
		return self::$paramTypes;
	}
	
	/* Perform final class cleanup (e.g. destroy arrays) */
	public static function finish() {
		self::$paramNames = null;
		self::$paramTypes = null;
		self::$paramValues = null;
		self::$isInit = false;
	}

	/* Perform a fail */
	public static function fail($errorMessage, $statusCode = 400, $errorCode = null) {
		switch ($statusCode) {
			case 400:
				$statusMessage = 'HTTP/1.1 400 Bad Request';
				break;
			case 500:
				$statusMessage = 'HTTP/1.1 500 Internal Server Error';
				break;
			default:
				$statusMessage = "HTTP/1.1 $statusCode";
				break;
		}
		header($statusMessage, true, $statusCode);

		$result = new stdClass();
		$result->statusCode = $statusCode;
		if ($errorCode) {
			$result->errorCode = $errorCode;
		}
		$result->errorMsg = $errorMessage;
		echo json_encode($result);
	}
}
?>