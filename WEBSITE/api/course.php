<?php
require 'functions/Model.php';

$db = new DB();
Utility::init();

// Add param ID if present
if (isset($_GET['id'])) {
	$id = Utility::toInt(Utility::getParam('id'));
	if ($id != null) {
		Utility::addParam('id', $id, PDO::PARAM_INT);
	}
}

/* Get course with ID = id */
$query = 'SELECT * FROM corsi WHERE ID = :id';

if (Utility::hasParameters()) {
	$courses = $db->query($query, Utility::getParamNames(), Utility::getParamValues(), Utility::getParamTypes());

    /* Get all instructors of the course */
    $query = 'SELECT IST.ID, IST.Nome, IST.Cognome, IST.Foto, IST.Bio FROM teaches_1 T1 JOIN istruttori IST ON T1.Istruttore = IST.ID WHERE T1.Corso = :id ORDER BY IST.Cognome, IST.Nome';
    $instructors = $db->query($query, Utility::getParamNames(), Utility::getParamValues(), Utility::getParamTypes());
    if (count($instructors) > 0) {
		foreach ($courses as $course) {
			$course->instructors = $instructors;
		}
	}

	// error handling
	if (!$courses) {
		Utility::fail("Error retrieving course with id $id", 500);
	} else {
    	echo json_encode($courses);
    }

} else {
	Utility::fail('No parameter specified', 400);
}

$db = null;
Utility::finish();
?>
