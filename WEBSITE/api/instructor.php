<?php
require 'functions/Model.php';

$db = new DB();
Utility::init();

// ID
if (isset($_GET['id'])) {
	$id = Utility::toInt(Utility::getParam('id'));
	if ($id != null) {
		Utility::addParam('id', $id, PDO::PARAM_INT);
	}
}

$query = 'SELECT * FROM istruttori WHERE ID = :id';
if (Utility::hasParameters()) {
	/* Get all instructors */
	$instructors = $db->query($query, Utility::getParamNames(), Utility::getParamValues(), Utility::getParamTypes());
	
	/* Get all teached courses*/
	$query = '	SELECT CO.ID, CO.Nome, CO.AddressedTo, L.Nome AS \'Livello\', CA.Nome AS \'NomeCategoria\'
				FROM ((teaches_1 T1 JOIN corsi CO ON T1.Corso = CO.ID)
					JOIN categorie CA ON CA.ID = CO.Categoria)
					JOIN livelli L ON CO.Livello = L.ID
				WHERE T1.Istruttore = :id
				ORDER BY CA.Nome, CO.Nome';
	$courses = $db->query($query, Utility::getParamNames(), Utility::getParamValues(), Utility::getParamTypes());
	if (count($courses) > 0) {
		foreach ($instructors as $instructor) {
			$instructor->courses = $courses;
		}
	}
	
	/* Get all prizes */
	$query = 'SELECT P.* FROM istruttori I JOIN prizes P ON I.ID = P.Istruttore WHERE I.ID = :id';
	$prizes = $db->query($query, Utility::getParamNames(), Utility::getParamValues(), Utility::getParamTypes());
	if (count($prizes) > 0) {
		foreach ($instructors as $instructor) {
			$instructor->awards = $prizes;
		}
	}

	if (!$instructors) {
		Utility::fail("Failed retrieving instructor with id $id", 500);
	} else {
		echo json_encode($instructors);
	}
} else {
	Utility::fail('No parameter specified', 400);
}

$db = null;
Utility::finish();
?>
