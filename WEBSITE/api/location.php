<?php
require 'functions/Model.php';

$db = new DB();

$query = 'SELECT * FROM location;';
$result = $db->query($query);

if (count($result) > 0) {
	echo json_encode($result[0]);
} else {
	Utility::fail('Failed to retrieve location from database', 500);
}

$db = null;
?>