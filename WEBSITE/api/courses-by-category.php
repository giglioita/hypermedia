<?php
require 'functions/Model.php';

$db = new DB();
Utility::init();

// ID
if (isset($_GET['id'])) {
	$id = Utility::toInt(Utility::getParam('id'));
	if ($id != null) {
		Utility::addParam('id', $id, PDO::PARAM_INT);
	}
}

$query = 'SELECT CO.*, L.Nome AS \'NomeLivello\' FROM corsi CO JOIN livelli L ON CO.Livello = L.ID WHERE CO.Categoria = :id ORDER BY CO.Nome, CO.Livello ASC;';

if (Utility::hasParameters()) {
	$corsi = $db->query($query, Utility::getParamNames(), Utility::getParamValues(), Utility::getParamTypes());
	$query = 'SELECT CA.Nome, CA.Slogan FROM categorie CA WHERE CA.ID = :id';
	$nomeCategoria = $db->query($query, Utility::getParamNames(), Utility::getParamValues(), Utility::getParamTypes());
	
	/* $nomeCategoria could be null */
	if ($nomeCategoria && $nomeCategoria[0]) {
		foreach ($corsi as $corso) {
			$corso->infoCategoria = $nomeCategoria[0];
		}
	}

	if (!$corsi) {
		Utility::fail("Error retrieving courses with category $id", 500);
	} else {
		echo json_encode($corsi);
	}
} else { // no id specified
	Utility::fail('No parameter specified', 400);
}

$db = null;
Utility::finish();
?>