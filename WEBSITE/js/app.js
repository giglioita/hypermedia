var app = angular.module('Hypermedia', ['ngRoute']);

/* Create a constant called API_ROOT to provide the path to "api" folder on server */
app.config(['$provide', function ($provide) {
    $provide.constant('API_ROOT', /* $('base').attr('href') + */ 'http://friendsfindermwd.altervista.org/' + 'api/');
}]);

/* Setup ngRoute for angular */
app.config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {
    /* To provide "back" and "forward" history on browser */
    $locationProvider.html5Mode(true);

    /* Setup routes */
    $routeProvider
        .when('/', {
            controller: 'HomeController',
            controllerAs: 'homeCtrl',
            templateUrl: 'partials/home.html'
        })
        .when('/index.html', {
            redirectTo: '/'
        })
        .when('/location', {
            controller: 'LocationController',
            controllerAs: 'locationCtrl',
            templateUrl: 'partials/location.html'
        })
        .when('/coursesByLevel', {
            controller: 'CoursesByLevelController',
            controllerAs: 'CoursesByLevelCtrl',
            templateUrl: 'partials/courses-by-lvl.html'
        })
        .when('/coursesAZ', {
            controller: 'CoursesAZController',
            controllerAs: 'CoursesAZCtrl',
            templateUrl: 'partials/courses-a-z.html'
        })
        .when('/allCategories', {
            controller: 'AllCategoriesController',
            controllerAs: 'AllCategoriesCtrl',
            templateUrl: 'partials/all-categories.html'
        })
        .when('/coursesByCategory/:id', {
            controller: 'CoursesByCategoryController',
            controllerAs: 'CoursesByCategoryCtrl',
            templateUrl: 'partials/courses-by-category.html'
        })
        .when('/instructor/:id', {
            controller: 'InstructorController',
            controllerAs: 'InstructorCtrl',
            templateUrl: 'partials/instructor.html'
        })
        .when('/course/:id', {
            controller: 'CourseController',
            controllerAs: 'CourseCtrl',
            templateUrl: 'partials/course.html'
        })
        .when('/courseCategory/:id', {
            controller: 'CourseCategoryController',
            controllerAs: 'CourseCategoryCtrl',
            templateUrl: 'partials/course-category.html'
        })
        .otherwise({
            templateUrl: 'partials/error404.html',
            controller: 'ErrorController',
            controllerAs: 'errorCtrl'
        });
}]);