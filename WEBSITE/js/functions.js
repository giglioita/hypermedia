/* Callback called when DOM has been loaded */
$(function () {
    /* Adjust height of some elements */
    adjustHeight();

    /* When the user clicks on a navbar element close the navbar, unless it is a dropdown */
    var navbar = $('.navbar-collapse');
    navbar.on('click', function (event) {
        if (!navbar.hasClass('collapse') && !$(event.target).hasClass("dropdown-toggle")) {
            navbar.collapse('hide');
        }
    });

    /* When the user clicks outside of the navbar, close it (but only if it is open) */
    $(document).on('click', function (event) {
        var navbar = $('.navbar-collapse');
        if (navbar.attr('aria-expanded') === 'true') {
            navbar.collapse('hide');
        }
    });

    /* Enable collapsing of navbar when user clicks on big gym logo */
    $('.logo').on('click', function (event) {
        event.preventDefault();
        var navbar = $('.navbar-collapse');
        if (navbar.attr('aria-expanded') === 'true') {
            navbar.collapse('hide');
        }

        window.location.href = './index.html';
    });

});

/* Add 'format' method to String class;
 * Now it is possible to use
 * 'this is a {0}'.format('string') to get
 * 'this is a string'.
 * Placeholders MUST start from 0
 */
String.prototype.format = function () {
    var s = this;
    var i = arguments.length;

    while (i--) {
        s = s.replace(new RegExp('\\{' + i + '\\}', 'mg'), arguments[i]);
    }

    return s;
}

/* Calculate the position of the stripes in the "home" fragment */
function calcPosizioneStripes() {

    if ($('#stripe1').length <= 0) {
        // we're not in the home page, so don't waste time
        return;
    }

    // centerY of the page
    var centerY = ($(window).height() - $(".navbar").height()) / 2;
    // height of a stripe (all stripes have same height)
    var stripeHeight = $("#stripe1").height();
    // we add some padding between consecutive stripes
    var padding = 8;
    // Y position of the upper stripe
    var startPoint = (centerY + 4) - 2 * (padding + stripeHeight);

    for (var i = 1; i <= 4; i++) {
        // every stripe has a progressive id: #stripe0, #stripe1, ...
        var stripe = $("#stripe{0}".format(i));

        /* Show stripes only if there is enough room in the page */
        if ($(window).height() > $(".navbar").height() + 4 * (stripeHeight + padding) + 50) {
            stripe.css({
                "top": startPoint + i * (padding - 1 + stripeHeight)
            }).fadeIn();
        } else {
            stripe.fadeOut();
        }
    }

}

/* Add some padding to the body, because of the navbar */
function calcPaddingBody() {
    $('body').css('padding-top', $('nav').height());
}

/* This function groups some height-related functions (e.g. the one that calculates the position of the stripes) */
function adjustHeight() {
    calcPosizioneStripes();
    calcPaddingBody();
}

/*****************************
 *     UTILITY FUNCTIONS     *
 ****************************/

/* returns true only if device is xs */
function isXsDevice() {
    return $(document).width() < 768;
}

/* returns true only if device is sm */
function isSmDevice() {
    return $(document).width >= 768 && $(document).width() < 992;
}

/* Whenever the screen has been resized, call the height-adjusting functions */
$(window).resize(adjustHeight);
