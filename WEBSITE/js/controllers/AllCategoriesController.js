/* Factory to retrieve course categories data from DB */
app.factory('AllCategoriesFactory', ['$http', 'API_ROOT', function ($http, apiRoot) {
    var result = {};
    result.data = function () {
        return $http.get('{0}all-categories.php'.format(apiRoot));
    }
    return result;
}]);

app.controller('AllCategoriesController', ['$scope', 'AllCategoriesFactory', function ($scope, AllCategoriesFactory) {
    var categorie;
    /* Calculate number of elements per row according to window resolution */
    var calcElemPerRow = function() {
    	if ($(window).width() >= 992)
	    	return 5;
	    else
	    	return 2;
	}

    /* Calculate number of rows to show categories boxes */
	var calcNumRighe = function() {
		$scope.categories = function(row) {
            /* Return only calcElemPerRow elements in order to generate rows with calcElemPerRow elements */
        	return categorie.slice(row*calcElemPerRow(), (row+1)*calcElemPerRow());
        }

        /* Generate not null rows in order to use ng-repeat in html */
        $scope.rows = new Array(categorie.length / calcElemPerRow());
        for (var i = 0; i < categorie.length / calcElemPerRow(); i++)
            $scope.rows[i] = i;
    }

    /* Retrieve data of categories */
    AllCategoriesFactory.data().success(function (response) {
        categorie = response;
        calcNumRighe();
    });

    /* Calculate number of rows on window resize */
    $(window).resize(function() {
    	calcNumRighe();
    	$scope.$apply();
    });

}]);