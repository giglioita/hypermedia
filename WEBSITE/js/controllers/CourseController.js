app.factory('CourseFactory', ['$http', '$routeParams', 'API_ROOT', function ($http, $routeParams, apiRoot) {
    var result = {};
    result.data = function () {
        return $http.get('{0}course.php?id={1}'.format(apiRoot, $routeParams.id));
    }
    return result;
}]);

app.controller('CourseController', ['$scope', 'CourseFactory', function ($scope, CourseFactory) {

    CourseFactory.data().success(function (response) {

            if (response[0]) {
                var corso = response[0];
                if (corso.Livello == 1) {
                    corso.Livello = 'Beginner';
                } else if (corso.Livello == 2) {
                    corso.Livello = 'Intermediate';
                } else if (corso.Livello == 3) {
                    corso.Livello = 'Advanced';
                }

                /* All the graphic elements are organized in the html file, they show the information about the course and the instructors we assign to the page here */
                $scope.course = corso;

                if (corso.AddressedTo != null) {
                    $scope.instructors = corso.instructors;

                    /* Check if the course has one ore more instructors and set the position for each instructor image */
                    if (corso.instructors.length == 1) {
                        $scope.instructorsTitle = 'Instructor';
                        $scope.imgClassSM = "col-sm-offset-3 ";
                        $scope.imgClassXS = "col-xs-offset-3 ";
                    } else if (corso.instructors.length == 2) {
                        $scope.instructorsTitle = 'Instructors';
                        $scope.imgClassSM = "";
                        $scope.imgClassXS = "";
                    }
                }
            }

        })
        .error(function (data, status, header, config) {
            // Failed to retrieve data from DB
            console.error(data, status);
            $scope.error = data;
        });

}]);
