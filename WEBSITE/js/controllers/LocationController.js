/* Create a factory to retrieve data from server.
 * It will return an object, whose "data" property contains the data.
 */
app.factory('LocationFactory', ['$http', 'API_ROOT', function ($http, apiRoot) {
    var result = {};
    result.data = function () {
        return $http.get('{0}location.php'.format(apiRoot));
    }
    return result;
}]);

/* Controller for the partial */
app.controller('LocationController', ['$scope', 'LocationFactory', '$sce', function ($scope, $factory, $sce) {
    
    /* Get the address */
    $factory.data()
        .success(function (data) {
            /* We must trust the html to show it (because there is an <a> tag for the email) */
            $scope.address = $sce.trustAsHtml(data.address);
        })
        .error(function (data, statusCode) {
            $scope.error = data;
        });

    /* Actual width of the window */
    var width = $(window).width();
    /* Old width of the window. Initially old = actual */
    var oldWidth = width;

    // the google map
    var map;

    // Big Gym coordinates
    var center = new google.maps.LatLng(45.4778322, 9.2274315);

    var mapOptions = {
        zoom: 15,
        /* Pretty close to the ground */
        center: center,
        /* Center the map on the gym */
        draggable: false /* The marker is not draggable */
    };

    // Create marker
    var marker = new google.maps.Marker({
        position: center,
        /* Positionate the marker on the gym */
        animation: google.maps.Animation.DROP,
        /* A little animation */
        title: 'Big Gym' /* Title of the marker */
    });

    // overlay for the marker, shown whenever the user clicks on it
    var infowindow = new google.maps.InfoWindow({
        content: '<b>Big Gym</b>' /* We display only the name of the gym */
    });

    // Holds the listener to center the map
    var centerListener;

    // Holds the marker click listener;
    var markerClickListener;

    // Holds the map click listener
    var mapClickListener;

    /* configure the map */
    function initialize() {

        // Get current visible map container
        var mapDiv = $('.map-canvas:visible')[0];

        // Create map
        map = new google.maps.Map(mapDiv,
            mapOptions);

        // Add marker to map
        marker.setMap(map);

        /* When the user clicks the marker we show some content */
        if (markerClickListener === undefined) {
            markerClickListener = google.maps.event.addListener(marker, 'click', function () {
                infowindow.open(map, marker);
            });
        }

        /* When the user resizes the screen we center the map again */
        if (centerListener === undefined) {
            centerListener = google.maps.event.addDomListener(window, 'resize', function () {
                // Center the map 
                map.setCenter(center);

                // Adjust map height
                calcAltezzaMappa();

                // new width
                width = $(window).width();

                // If resize makes the window move from xs to sm/md/lg or viceversa, then reinit the map
                if ((oldWidth >= 768 && width < 768) || (oldWidth < 768 && width >= 768)) {
                    // re-init map, because it is shown in another container
                    initialize();
                    oldWidth = width;
                }
            });
        }

        /* When the user clicks on the map, (s)he will be able to scroll it */
        if (mapClickListener === undefined) {
            mapClickListener = google.maps.event.addListener(map, 'click', function () {
                // If it's not draggable, enable drag
                if (!this.draggable) {
                    this.setOptions({
                        draggable: true /* now it is draggable */
                    });
                }
            });
        }
    }


    /* Calculate initial position of the map. This must be done BEFORE initializating the map,
     * otherwise the map will appear grey due to a bug in google maps
     * (see http://gis.stackexchange.com/questions/32290/google-maps-openlayers-only-rendering-top-left-tiles)
     */
    calcAltezzaMappa();

    /* Calculate the position of social buttons */
    calcPosizioneLoghi();

    /* Let's init the map */
    initialize();

    // remove all listeners when user changes page
    var remover = $scope.$on('$locationChangeStart', function () {
        google.maps.event.removeListener(markerClickListener);
        google.maps.event.removeListener(centerListener);
        google.maps.event.removeListener(mapClickListener);
        remover(); // remove $locationChangeStart event listener
    });
}]);

/* This function adjust social buttons y-position */
function calcPosizioneLoghi() {
    var h1Height = $('h1').outerHeight(true);

    $('#loghi').css('margin-top', h1Height);
}

/* Let the map be as tall as left column */
function calcAltezzaMappa() {

    var h1Height = $('h1').outerHeight(true);
    /* Set margin-top to map only on .sm or greater devices */
    if (!isXsDevice()) {
        $('.map-canvas').css('margin-top', h1Height);
    } else {
        $('.map-canvas').css('margin-top', 0);
    }

    var exists = $('.map-canvas').length > 0;
    if (exists) {
        $('.map-canvas').height($('#left-col').height() - 2 - h1Height); /* There are 2 px of margin */
    } else {
        console.log('Map doesn\'t exist');
    }
}