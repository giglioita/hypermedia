/* Factory to retrieve data from DB */
app.factory('InstructorFactory', ['$http', '$routeParams', 'API_ROOT', function ($http, $routeParams, apiRoot) {
    var result = {};
    result.data = function () {
        return $http.get('{0}instructor.php?id={1}'.format(apiRoot, $routeParams.id));
    }
    return result;
}]);

app.factory('InstructorFBPhotosFactory', ['$http', '$routeParams', 'API_ROOT', function ($http, $routeParams, apiRoot) {
    var result = {};
    result.data = function () {
        return $http.get('{0}graph.php?id={1}'.format(apiRoot, $routeParams.id));
    }

    return result;
}]);

app.factory('InstructorTweetsFactory', ['$http', '$routeParams', 'API_ROOT', function ($http, $routeParams, apiRoot) {
    var result = {};
    result.getData = function () {
        return $http.get('{0}twitter.php?id={1}'.format(apiRoot, $routeParams.id));
    }
    return result;
}]);

app.controller('InstructorController', ['$scope', '$sce', 'InstructorFactory', 'InstructorFBPhotosFactory', 'InstructorTweetsFactory', function ($scope, $sce, InstructorFactory, PhotosFactory, TweetsFactory) {
    /* We show only 3 pics for each instructor */
    var numberOfPics = 3;

    /* Instructor data, such as bio, qualifications, ... */
    data = InstructorFactory.data();
    data.success(function (data) {

            /*
             * We add a property to denote that the i-th course belongs to different category
             * in respect to the previous one.
             */
            if (data[0].courses) {
                var previous = null;
                for (var i = 0; i < data[0].courses.length; i++) {
                    if (data[0].courses[i].NomeCategoria !== previous) {
                        data[0].courses[i].first = true;
                    }

                    previous = data[0].courses[i].NomeCategoria;
                }
            }

            $scope.instructor = data[0];
            $scope.courses = data[0].courses;

            $scope.imgArray; /* holds the pics of the photogallery */
            $scope.logged; /* Is the user logged in facebook? */

            /* If the instructor has Twitter, show his last post */
            if ($scope.instructor.TwitterID) {
                // Will load twitter now, because the WidgetID is set dynamically
                //loadTwitter(document, "script", "twitter-wjs");
                TweetsFactory.getData().success(function (tweets) {
                        var tweet = tweets[0];

                        // Create a link for http links
                        tweet.entities.urls.forEach(function (url) {
                            var regex = new RegExp(url.url);
                            tweet.text = tweet.text.replace(regex, '<a href={0}>{1}</a>'.format(url.url, url.display_url));
                        });

                        // Create a link for user mentions
                        tweet.entities.user_mentions.forEach(function (mention) {
                            var regex = new RegExp('\@' + mention.screen_name);
                            tweet.text = tweet.text.replace(regex, '<a href=http://twitter.com/{0}>@{0}</a>'.format(mention.screen_name));
                        });

                        // Render text as HTML
                        tweet.text = $sce.trustAsHtml(tweet.text);
                    
                        // Beautify date with angular "date" filter (needs date to be in milliseconds)
                        var millis = Date.parse(tweet.created_at);
                        tweet.created_at_millis = millis;
                        $scope.tweet = tweet;

                    })
                    .error(function (data) {
                        // failed loading tweets
                        $scope.tweetsError = data;
                        console.log(data);
                    });

            }

            /* If the instructor has FB, let's grab his/her images */
            if ($scope.instructor.FacebookID) {
                loadPhotos();
            } else if ($scope.instructor.Photogallery) {
                /* The instructor has no FB public page, let's load pics from the DB */
                $scope.imgArray = $scope.instructor.Photogallery.split(';');
            }

        })
        .error(function (data, status, header, config) {
            // Failed to retrieve instructor data from DB
            console.error(data, status);
            $scope.instructorError = data;
        });

    /* Center vertically the instructor image inside its container */
    var marginTop = ($("#courses-container").height() - $('#instructor-image').height()) / 2;
    $("#instructor-image").css('margin-top', marginTop);

    /* Load photos from instructor's FB page */
    function loadPhotos() {
        PhotosFactory.data().success(function (photosArray) {
                // These arrays will contain the photos
                $scope.imgArray = [];
                $scope.imgArrayOriginal = [];

                // Fill the arrays
                for (var i = 0; i < photosArray.length; i++) {
                    $scope.imgArray.push(photosArray[i].small);
                    $scope.imgArrayOriginal.push(photosArray[i].original);
                }

            })
            .error(function (data) {
                $scope.photosError = data;
                console.error(data);
            });
    }

    /* delete twitter script in order to re-execute twitter-wjs code */
    var remover = $scope.$on('$locationChangeStart', function () {
        $('#twitter-wjs').remove();
        remover(); // remove the listener when leaving the page
    });
}]);