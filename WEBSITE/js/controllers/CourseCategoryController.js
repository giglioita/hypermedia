/* Factory to retrieve course category data from DB */
app.factory('CourseCategoryFactory', ['$http', '$routeParams', 'API_ROOT', function ($http, $routeParams, apiRoot) {
    var result = {};
    result.data = function () {
        return $http.get('{0}category.php?id={1}'.format(apiRoot, $routeParams.id));
    }
    return result;
}]);

app.controller('CourseCategoryController', ['$scope', '$sce' , 'CourseCategoryFactory', function ($scope, $sce, CourseCategoryFactory) {
	/* Retrieve course category data */
	CourseCategoryFactory.data().success(function (response) {
		$scope.category = response;
		$scope.category.videos = [];
	    $scope.courses = response.courses;

	    /* Calculate how many columns associate to each instructor's picture */
	    $scope.istNumCols = 12 / response.instructors.length;

	    /* Generate urls of course category's youtube videos */
	    for (var i = 0; i < response.Video.split(";").length; i++)
	    	$scope.category.videos[i] = $sce.trustAsResourceUrl('http://www.youtube.com/embed/' + response.Video.split(";")[i]);
	})
	.error(function (data, status, header, config) {
            // Failed to retrieve data from DB
            console.error(data, status);
            $scope.error = data;
    });

}]);