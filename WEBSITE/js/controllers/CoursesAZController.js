app.factory('CoursesAZFactory', ['$http', 'API_ROOT', function ($http, apiRoot) {
    var result = {};
    result.data = function () {
        return $http.get('{0}courses-a-z.php'.format(apiRoot));
    }
    return result;
}]);

app.controller('CoursesAZController', ['$scope', 'CoursesAZFactory', '$sce', function ($scope, CoursesAZFactory, $sce) {

    /* Function that calculates the number of stars to be displayed for each course */
    var stars = function (level) {
        if (level == 1) {
            return '&#9733';
        } else if (level == 2) {
            return '&#9733 &#9733';
        } else if (level == 3) {
            return '&#9733 &#9733 &#9733';
        }
    }

    CoursesAZFactory.data().success(function (response) {

        if (response) {
            var corsi = response;
            var coursesArrays = [];
            var sameLetterCourses = [];
            var initials = [];
            var initial = null;

            for (var i = 0; i < corsi.length; i++) {
                if (corsi[i].NomeCorso.charAt(0) != initial) {
                    initial = corsi[i].NomeCorso.charAt(0);
                    initials.push(initial);

                    /* If the course is not the first one and it has an initial different from the previous course, we add the previous array of courses with same initial to the array of arrays */
                    if (sameLetterCourses.length > 0) {
                        coursesArrays.push(sameLetterCourses);
                        sameLetterCourses = [];
                    }
                }

                /* We build the html (name + as many stars as the level) to show for each course, this html will be trusted before being shown */
                corsi[i].Label = $sce.trustAsHtml(corsi[i].NomeCorso + " " + stars(corsi[i].LivelloCorso));

                /* We add the course to its related array of courses with same initial */
                sameLetterCourses.push(corsi[i]);
            }

            /* We add the last array of courses to the array of arrays */
            if (sameLetterCourses.length > 0) {
                coursesArrays.push(sameLetterCourses);
            }

            /* Array of initials */
            $scope.initials = initials;

            /* Array of arrays containing courses with same initial */
            $scope.coursesArrays = coursesArrays;

            /* One third, two thirds and a half value of the length of the initials array, needed to display properly the courses inside the table */
            $scope.oneThirdInitials = Math.floor(initials.length / 3);
            $scope.twoThirdsInitials = 2 * Math.floor(initials.length / 3);
            $scope.aHalfInitials = Math.floor(initials.length / 2);
        }
    });

}]);
