app.factory('CoursesByCategoryFactory', ['$http', '$routeParams', 'API_ROOT', function ($http, $routeParams, apiRoot) {
    var result = {};
    result.data = function () {
        return $http.get('{0}courses-by-category.php?id={1}'.format(apiRoot, $routeParams.id));
    }
    return result;
}]);

/* We create a directive to run a callback when the ngRepeat directive is ending */
app.directive('watchEnd', ['$timeout', function ($timeout) {
    return {
        restrict: 'A',
        link: function (scope) {
            if (scope.$last) {
                $timeout(function () {
                    $('tr td img').load(function () {
                        var difficulty = $('.vertical-text'); /* reference to <h3> */
                        difficulty.each(function (index) {
                            $(this).width($('table tr:nth-child({0}) td img'.format(index + 1)).height()) /* set width equal to <img> */
                                .css('right', -$(this).width()); /* Align bottom-left corner of the <h3> with bottom-right corner of the <tr> */
                        })

                        /* There is a bug in firefox known as 'pixel snapping' */
                        .css('bottom', 0) /* see comment below */
                            .css('transform-origin', '0% 100%') /* Set bottom-left corner of the <h3> as the pivot */
                            .css('transform', 'rotate(-90deg)') /* Perform 90 degrees rotation counter-clockwise */
                            .css('-moz-transform', 'rotate(-90deg)')
                            .css('-moz-transform-origin', '0% 100%')
                            .css('-webkit-transform', 'rotate(-90deg)')
                            .css('-webkit-transform-origin', '0% 100%');
                    });
                });
            }
        }
    }
}]);

/* Return the color associated to the difficulty */
function getColor(level) {
    level = parseInt(level);
    switch (level) {
    case 1:
        return 'yellow';
    case 2:
        return 'orange';
    case 3:
        return 'red';
    default:
        /* return nothing */
        return;
    }
};

app.controller('CoursesByCategoryController', ['$scope', '$routeParams', '$location', '$timeout', 'CoursesByCategoryFactory', function ($scope, $routeParams, $location, $timeout, factory) {
    /* Make table rows clickable */
    $('.table').on('click', '.clickable-row', function () {
        var href = $(this).data('href');
        var res = $location.path(href);
        $scope.$apply();
    });

    /* Get the background color associated to the difficulty */
    $scope.getBackgroundColor = function (level) {
        return {
            'background-color': getColor(parseInt(level))
        };
    }

    /* Get the text color associated to the difficulty */
    $scope.getTextColor = function (level) {
        return {
            'color': getColor(parseInt(level))
        };
    }

    /* Get data from factory */
    factory.data()
        .success(function (response) {
            $scope.category = response[0].infoCategoria;
            $scope.courses = response;
        })
        .error(function (data, status, header, config) {
            var error = {};
            error.data = data;
            error.code = status;
            $scope.error = error;
        });



}]);