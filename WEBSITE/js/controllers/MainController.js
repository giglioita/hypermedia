app.controller('MainController', ['$scope', function ($scope) {
    
    /*
     * When the user leaves the page, remove all $(window).resize listeners and
     * re-enable only the one who computes the distance between the body and the navbar
     */
    $scope.$on('$locationChangeSuccess', function () {
        // Remove all existent listeners
        $(window).off('resize');
        
        // Re-enable main resize listener
        $(window).resize(adjustHeight);
    });
}]);