app.controller("ErrorController", function ($scope, $timeout, $location) {

    $scope.counter = 5;
    $scope.onTimeout = function () {
        // if timeout has expired then redirect
        if ($scope.counter <= 0) {
            $location.path("/");
        } else {
            $scope.counter--;
            _timeout = $timeout($scope.onTimeout, 1000);
        }
    };

    // if the user clicks on redirect then cancel timeout
    $("#redirect").click(function () {
        var canceled = $timeout.cancel(_timeout);
    });

    // start timeout
    var _timeout = $timeout($scope.onTimeout, 1000);
});