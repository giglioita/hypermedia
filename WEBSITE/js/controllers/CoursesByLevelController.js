app.factory('CoursesByLevelFactory', ['$http', 'API_ROOT', function ($http, apiRoot) {
    var result = {};
    result.data = function () {
        return $http.get('{0}courses-by-lvl.php'.format(apiRoot));
    }
    return result;
}]);

app.controller('CoursesByLevelController', ['$scope', 'CoursesByLevelFactory', function ($scope, CoursesByLevelFactory) {

    CoursesByLevelFactory.data().success(function (response) {

        if (response) {
            var categorie = response;
            var begCategories = [];
            var intCategories = [];
            var advCategories = [];

            for (var i = 0; i < categorie.length; i++) {

                /* For each category, we add it to bgCategories, intCategories or/and advCategories if it contains courses with these levels */
                if (categorie[i].corsiBeginner.length > 0) {
                    begCategories.push(categorie[i]);
                }
                if (categorie[i].corsiIntermediate.length > 0) {
                    intCategories.push(categorie[i]);
                }
                if (categorie[i].corsiAdvanced.length > 0) {
                    advCategories.push(categorie[i]);
                }
            }

            /* Bindings needed to build the menus for extra-small screens and the table for larger screens */
            $scope.begCategories = begCategories;
            $scope.intCategories = intCategories;
            $scope.advCategories = advCategories;
            $scope.allCategories = categorie;
        }

    });

}]);
