-- phpMyAdmin SQL Dump
-- version 4.1.7
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Giu 14, 2015 alle 23:09
-- Versione del server: 5.1.71-community-log
-- PHP Version: 5.3.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `my_friendsfindermwd`
--
CREATE DATABASE IF NOT EXISTS `my_friendsfindermwd` DEFAULT CHARACTER SET latin1 COLLATE latin1_general_ci;
USE `my_friendsfindermwd`;

-- --------------------------------------------------------

--
-- Struttura della tabella `categorie`
--

CREATE TABLE IF NOT EXISTS `categorie` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Nome` varchar(45) COLLATE latin1_general_ci NOT NULL,
  `Copertina` text COLLATE latin1_general_ci,
  `Foto` text COLLATE latin1_general_ci,
  `Video` text COLLATE latin1_general_ci,
  `intro` text COLLATE latin1_general_ci,
  `origins` text COLLATE latin1_general_ci,
  `goodfor` text COLLATE latin1_general_ci,
  `forwho` text COLLATE latin1_general_ci,
  `Slogan` text COLLATE latin1_general_ci,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=11 ;

--
-- Dump dei dati per la tabella `categorie`
--

INSERT INTO `categorie` (`ID`, `Nome`, `Copertina`, `Foto`, `Video`, `intro`, `origins`, `goodfor`, `forwho`, `Slogan`) VALUES
(1, 'Aerobics', 'aerobics.jpg', NULL, NULL, '', '', '', '', NULL),
(2, 'Aquafitness', 'acquafitness-img.jpg', NULL, 'zDyP56oKFZY;9MLUkJGJ1so', 'Aquafitness is the performance of aerobic exercise in fairly shallow water such as in a swimming pool. Done mostly vertically and without swimming typically in waist deep or deeper water, it is a type of resistance training. Most water aerobics is in a group fitness class setting with a trained professional teaching for about an hour.', 'The first organized form of water aerobics was practiced by television fitness professional Jack LaLane during the 1950s. Viewed in thousands of households, LaLane''s daily television shows promoted a healthy diet and the benefits of aerobic exercise. As interest in health and fitness rose in the 1970s and 1980s, aquatic aerobics began gaining in popularity. The benefits of a low-impact aerobic exercise program in the water became more apparent and aquatic exercise became the exercise program of choice for athletes rehabilitating from injuries, patients recovering from surgery and the elderly.', 'The classes focus on aerobic endurance, resistance training, and creating an enjoyable atmosphere with music. Working out in waist- or chest-deep water allows you to exercise nearly every muscle and joint in your body. In addition, the water helps cool your body to keep you from getting too hot. Because of the buoyancy of the water, you only have to support about half of your body weight if you''re in waist-deep water. This allows you to exercise at a greater intensity without risk of joint injury.', 'People of every kind of age and physical ability can enjoy aquafitness classes and gain benefits.', NULL),
(3, 'Bodybuilding', 'body-building.jpg', NULL, NULL, '', '', '', '', NULL),
(4, 'Cardio', 'cardio-img.jpg', NULL, 'H7kpel5opRE;KOPT66_KqVU', 'Cardio exercise is any exercise that raises your heart rate. Face it our bodies were made to move. And we all know that to keep our muscles in shape we need move them. This movement makes them stronger and stronger muscles make for a more efficient and healthy body. Your heart is a muscle. Therefore working it makes it stronger. A stronger cardio-vascular system means more capillaries delivering more oxygen to cells in your muscles. This enables your cells to burn more fat during both exercise and inactivity.', 'In the 1960s, Dr. Kenneth H. Cooper developed a system of exercises to prevent coronary artery sickness. The system was developed at the Air Force Hospital and originally intended for those in the military. He termed it “aerobics” in a book of the same name that he published in 1968. After publishing, dancer Jackie Sorenson developed dance routines that aimed at improving cardio fitness, which were coined aerobic dance.', 'Cardio has a lot of advantages: weight loss, stronger heart and lungs, increased bone density, reduced stress, reduced risk of heart disease and some types of cancer, temporary relief from depression and anxiety, more confidence about how you feel and how you look, better sleep, more energy, setting a good example for your kids to stay active as they get older.', 'Cardio training is suitable for people of any age or physical condition. Overweight person should avoid excercising too quickly for the first times, but approach the excercises in a moderate way, increasing the difficulty gradually. ', NULL),
(5, 'Children', 'children.jpg', NULL, 'YO6mW_mrtBg;WOSfF8JwaC8', 'Our gymnastics program for preschoolers and kindergarteners has been specially designed to help your child channel all that energy and reach developmental milestones. Independent enough to attend classes without parents, children in this age group still learn best in a structured environment where gymnastics activities are combined with a healthy dose of fun.', 'Gymnastics classes are a popular activity for children in preschool and elementary school. While gymnastics can be traced to ancient Greece, modern gymnastics for children began for boys in the late 18th century and for girls in the 19th and early 20th centuries.', 'Gymnastics is an excellent activity for both boys and girls to become involved in. Gymnastics for children in their early years has proven to be an effective way to develop specific skills. The skills taught can improve balance, coordination and strength, and classes provide an opportunity for exercise and teaching self-discipline. Children can continue with gymnastics right through their childhood years.\r\nParticipation in gymnastics helps children become physically active, and stay fit and healthy. Taking part in any exercise significantly reduces the risk of obesity, heart disease, and diabetes in adulthood.\r\nAs children become older and more advanced with their gymnastics a particular body type is required to excel at the sport. It is recommended you discuss this matter with a qualified gymnastics instructor.', '', NULL),
(6, 'Dance', 'zumba.jpg', NULL, NULL, '', '', '', '', NULL),
(7, 'Mind&Body', 'yoga.jpg', NULL, NULL, '', '', '', '', NULL),
(8, 'Pilates', 'pilates.jpg', NULL, NULL, '', '', '', '', 'Find your shape!'),
(9, 'Spinning', 'spinning.jpg', NULL, 'Gx4tlk7A7SY;67voznKxm_E', 'Spinning is a group activity on the bike for spinning, where the instructor dictates the rhythm of pedaling.', 'Competitive cyclist Johnny Goldberg, popularly known as Johnny G., first created indoor cycling in 1986. While preparing for the Race Across America, he needed a form of cycling that would allow him to complete his training no matter what the weather. The answer to his dilemma was in the creation of a stationary bike with a weighted flywheel that remarkably simulated an outdoor cycle. Johnny G. benefited from his invention so much that he figured other people could, too. He took his idea to Schwinn, the most prominent American bicycle-manufacturing company, and the concept of indoor cycling exercise was born.', 'Spinning brings numerous benefits for your body and mind:\r\n- Superior cardiovascular workout (both aerobic and anaerobic) strengthens the heart and helps lower resting heart rate.\r\n- Indoor cycling exercise provides a great way to tone the glutes, quads and hamstrings.\r\n- Stabilizers and core muscles of the body get a good workout during the sitting and standing positions, which helps encourage good posture in everyday life.\r\n- Weight loss goals are reached more quickly, because a rider burns 400-600 calories in one 45-minute indoor cycling session.\r\n- Indoor cycling exercise is a great way to release stress and work off tension from your day.', 'Everyone can benefit from spinning: all ages and fitness levels will find it to be challenging and rewarding.', NULL),
(10, 'Swimming', 'swimming.png', NULL, 'ihYVQ-o1hxg;1WUWXDyYOWM', 'Swimming includes different activities among which all different swimming styles (freestyle, bakstroke, breaststroke, butterfly stroke) and an exclusive lifeguard course held by one of the most popular instructor of the world, David Hasselhoff.', 'The recreational activity of swimming has been recorded since prehistoric times. The earliest recording of swimming dates back to Stone Age paintings from around 10000 years ago. Written references date from 2000 BC. Some of the earliest references to swimming include the Iliad, the Odyssey, the Bible, Beowulf, The Quran along with others. In 1538, Nikolaus Wynmann, a German professor of languages, wrote the first swimming book, The Swimmer or A Dialogue on the Art of Swimming.\r\nSwimming emerged as a competitive recreational activity in the 1830s in England.', 'Swimming burns calories, so helps you lose weight and swimming for weight loss has been regularly praised for its cardiovascular health benefits, especially for older people.\r\nAs swimming uses so many muscles in your body, your heart and lungs must work hard to supply them all with oxygen. This means that swimming will give your cardiovascular system an excellent workout.', 'People who want to swim at a competitive level or loose weight.', 'Come swim with us!');

-- --------------------------------------------------------

--
-- Struttura della tabella `corsi`
--

CREATE TABLE IF NOT EXISTS `corsi` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Nome` varchar(45) COLLATE latin1_general_ci NOT NULL,
  `Livello` int(11) NOT NULL,
  `Categoria` int(11) NOT NULL,
  `AddressedTo` text COLLATE latin1_general_ci,
  `Description` text COLLATE latin1_general_ci,
  `Foto` text COLLATE latin1_general_ci,
  `Copertina` text COLLATE latin1_general_ci,
  PRIMARY KEY (`ID`),
  KEY `Livello` (`Livello`),
  KEY `Categoria` (`Categoria`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=45 ;

--
-- Dump dei dati per la tabella `corsi`
--

INSERT INTO `corsi` (`ID`, `Nome`, `Livello`, `Categoria`, `AddressedTo`, `Description`, `Foto`, `Copertina`) VALUES
(1, 'Abs Attack', 3, 8, NULL, NULL, NULL, 'abs_rect.png'),
(2, 'Aqua Flex', 2, 2, NULL, NULL, NULL, NULL),
(3, 'Aqua Zumba', 2, 2, NULL, NULL, NULL, NULL),
(4, 'Pilates', 1, 8, 'Pilates for beginners is suitable for people of all ages. It''s never too late to start. With pilates, you can devise a programme of exercises tailored to the individual.\nWith older adults, we tend to work on balance, posture, co-ordination and breathing. We offer more gentle exercises to work on their weaknesses and improve their mobility.', 'Pilates is a method of exercise that consists of low-impact flexibility and muscular strength and endurance movements. It emphasizes use of the abdominals, lower back, hips and thighs.\r\nBy practicing Pilates regularly, you can achieve a number of health benefits, including: improved core strength and stability, improved posture and balance, improved flexibility, prevention and treatment of back pain.', 'pilates.png', 'pilates_beg_rect.png'),
(5, 'Body Attack', 3, 4, NULL, NULL, NULL, NULL),
(6, 'Body Balance', 1, 7, NULL, NULL, NULL, NULL),
(7, 'Bodybuilding', 1, 3, 'Addressed to people who have decided to try out bodybuilding. Our trainers are extremely versatile and will accommodate clients whatever their goals may be.\nParticipants must be at least 13 years old.', 'Stop training like an idiot! "Dumbbell" should describe your weights, not you. Let the smartest man in bodybuilding take you from beginner to advanced in just 12 weeks!\nMaybe you''ve never lifted anything heavier than your coffee mug. Maybe you once lifted regularly, but of late, your trips to the gym have become as infrequent as a day of sobriety for Charlie Sheen.\nIn either case, we have good news, in the form of the perfect 12-week plan for going from beginner to advanced.', 'bodybuilding.jpg', NULL),
(8, 'Bodybuilding', 2, 3, NULL, NULL, NULL, NULL),
(9, 'Bodybuilding', 3, 3, NULL, NULL, NULL, NULL),
(10, 'Boot Camp', 3, 4, NULL, NULL, NULL, NULL),
(11, 'Dance For Kids', 1, 5, NULL, NULL, NULL, NULL),
(12, 'Deep Water', 3, 2, NULL, NULL, NULL, NULL),
(13, 'Gymnastic', 1, 5, NULL, NULL, NULL, NULL),
(14, 'Gymnastic', 2, 5, NULL, NULL, NULL, NULL),
(15, 'Lifeguard', 3, 10, 'Addressed to guys who are at least 16 years old and can: jump/dive into deep water, swim 50 m in less than 60 s, swim 100 m continuously on front and back in deep water, tread water for 30 s, surface dive to the floor of the pool at its deepest point, climb out unaided without ladder/steps', 'The course duration is 36-38 hours split over 3 Sections: 1.The Lifeguard, Swimming Pool and Supervision, 2.Intervention, Rescue and Emergency Action Plan and 3.CPR, AED and First Aid. The National Pool Lifeguard Qualification is awarded upon successful completion of all sections following assessment on all subjects.\nThe course comprises of physical training and theoretical classroom work, followed by a practical in-water and out-of-water assessment on pool rescue, first aid, CPR and lifesaving skills.', 'lifeguard.png', 'lifeguard_rect.png'),
(16, 'Low Impact Aerobics', 2, 1, NULL, NULL, NULL, NULL),
(17, 'Piloxing', 3, 8, NULL, NULL, NULL, 'piloxing_rect.png'),
(18, 'RPM', 3, 9, NULL, NULL, NULL, NULL),
(19, 'Running', 3, 4, NULL, NULL, NULL, NULL),
(20, 'Spinning', 1, 9, NULL, NULL, NULL, NULL),
(21, 'Spinning', 2, 9, NULL, NULL, NULL, NULL),
(22, 'Step', 1, 1, NULL, NULL, NULL, NULL),
(23, 'Swimming', 1, 10, 'Addressed to people who have very little or no swimming experience and may be fearful or uncomfortable in the water of any depth', 'The course will provide instruction in basic water skills including comfortable entry, submersion, floating, breathing techniques, and an introduction to basic swimming strokes. The course is intended to help participant gain confidence and self-reliance in the water.\r\nThe participants will learn: safe water entry and exit; to submerge, exhale, and open eyes underwater; basic breath control; to float on front and back; to change directions in the water; basic personal water safety skills.', 'swimming_beg.png', 'swimm_beg_rect.png'),
(24, 'Swimming', 2, 10, 'Addressed to people who are comfortable treading, floating and swimming in deep water, can swim front crawl with rotary breathing and can swim basic backstroke and breaststroke.\r\nNote: exceptions may be made to these prerequisites with approval of the instructor.', 'This course is designed to provide instruction in five basic swim strokes: front crawl (freestyle), back crawl (backstroke), breaststroke, elementary backstroke and sidestroke. Additional emphasis will be placed on increased cardiovascular fitness along with skill development in treading water, underwater swims, turns and dives.', 'swimming_int.png', 'swimm_int_rect.png'),
(25, 'Swimming', 3, 10, 'Addressed to guys who are confident in their swimming ability level in any water depth, and are interested in refining their stroke techniques and improving their endurance.\nParticipants must be able to tread water for 5 minutes, and swim at least 50 yards of front crawl, back crawl, and breaststroke.', 'In this class you will refine the 4 competitive swimming strokes (freestyle, breaststroke, backstroke, & review/intro to butterfly) and efficient breathing techniques. You will gain additional development of flip turns using intervals (100 yard repeats) and competitive starts and turns, as well as the use of swimming equipment (kick boards, pull buoys, hand paddles, fins) for fitness. Videotaping with review will be used to improve your strokes.', 'swimming_adv.png', 'swimm_adv_rect.png'),
(26, 'Tai Chi', 2, 7, NULL, NULL, NULL, NULL),
(27, 'U-Jam Fitness', 2, 6, NULL, NULL, NULL, NULL),
(28, 'Yoga', 2, 7, NULL, NULL, NULL, NULL),
(29, 'Zumba', 2, 6, NULL, NULL, NULL, NULL),
(30, 'Spinningfest', 3, 9, NULL, NULL, NULL, NULL),
(31, 'Spinning for Seniors', 1, 9, NULL, NULL, NULL, NULL),
(32, 'Acquafitness for Seniors', 1, 2, NULL, NULL, NULL, NULL),
(33, 'AquaTurbo', 3, 2, NULL, NULL, NULL, NULL),
(34, 'Aqua Aerobics', 1, 2, NULL, NULL, NULL, NULL),
(35, 'Bodyblade', 2, 4, NULL, NULL, NULL, NULL),
(36, 'Power 90', 2, 4, NULL, NULL, NULL, NULL),
(37, 'Preschool Prep', 1, 5, NULL, NULL, NULL, NULL),
(38, 'Mighty Mites', 3, 5, NULL, NULL, NULL, NULL),
(39, 'Finswimming', 3, 10, NULL, NULL, NULL, 'finswimming_rect.png'),
(40, 'Diving', 2, 10, NULL, NULL, NULL, 'diving_rect.png'),
(41, 'Barre Pilates', 1, 8, NULL, NULL, NULL, 'barre_pilates_rect.png'),
(42, 'PiYo', 1, 8, NULL, NULL, NULL, 'piyo_rect.png'),
(44, 'Pilates', 3, 8, NULL, NULL, NULL, 'pilates_adv_rect.png'),
(43, 'Pilates', 2, 8, NULL, NULL, NULL, 'pilates_int_rect.png');

-- --------------------------------------------------------

--
-- Struttura della tabella `istruttori`
--

CREATE TABLE IF NOT EXISTS `istruttori` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Nome` varchar(45) COLLATE latin1_general_ci NOT NULL,
  `Cognome` varchar(45) COLLATE latin1_general_ci NOT NULL,
  `Bio` text COLLATE latin1_general_ci,
  `Foto` text COLLATE latin1_general_ci,
  `Qualifications` text COLLATE latin1_general_ci,
  `Photogallery` text COLLATE latin1_general_ci,
  `FacebookID` text COLLATE latin1_general_ci,
  `WidgetID` text COLLATE latin1_general_ci,
  `TwitterID` varchar(15) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=13 ;

--
-- Dump dei dati per la tabella `istruttori`
--

INSERT INTO `istruttori` (`ID`, `Nome`, `Cognome`, `Bio`, `Foto`, `Qualifications`, `Photogallery`, `FacebookID`, `WidgetID`, `TwitterID`) VALUES
(1, 'Antoine', 'Vaillant', 'Antoine Vaillant was born on July 28, 1987 in Montreal, Canada.\r\nHe started working out by doing hockey for 8 years, then he transitioned to MMA and, finally, to bodybuilding.\r\nHis main interests outside body building are music (mainly metal and house music), TV series and videogames.', 'antoine-vaillant.png', 'Personal trainer certification released by National Gym Association in 2009\r\nBody Building instructor certificate released by United States Sports Academy.', NULL, 'AVbodybuilder', '601772407402160128', 'AntoineVai'),
(2, 'David', 'Hasselhoff', 'David Hasselhoff was born on July 17, 1952 in Baltimora, Maryland.\r\nHe worked as a lifeguard for 11 years (1989-2000) at Los Angeles County, California, rescuing hundreds of people.\r\nIn 1997 he has been promoted to Captain Lifeguard.\r\nIn 2015 he started working at Big Gym as a swim instructor.', 'david-hasselhoff.png', 'Lifeguard certification released by American Red Cross on 1988.\r\n\r\nSwim instructor certification released by American Red Cross on 2000.', NULL, 'davidhasselhoff', '603859404874764288', 'DavidHasselhoff'),
(3, 'Maddy', 'McLaren', 'Maddy McLaren was born June 21, 1994 in Newport Beach.\r\nShe started playing water polo in 2012 at Newport Harbor High School, where she won her first championship.\r\nShe then studied at UCLA, where she joined the UCLA Bruins, the UCLA sport team, where she played 53\r\nmatches and scored 17 goals.\r\nShe now teaches swimming courses (beginners and intermediate) at Big Gym.', 'maddy-mclaren.png', 'Lifeguard certification released by American Swim Instructor Academy on 2010.', 'maddy-mclaren01.jpg;maddy-mclaren02.jpg;maddy-mclaren03.jpg', NULL, NULL, NULL),
(4, 'Brennan', 'Clay', NULL, 'Brennan-Clay.png', NULL, NULL, NULL, NULL, NULL),
(5, 'Ed', 'Fringe', NULL, 'ED-FRINGE.png', NULL, NULL, NULL, NULL, NULL),
(6, 'Kate', 'Hiipakka', NULL, 'Kate-Hiipakka.png', NULL, NULL, NULL, NULL, NULL),
(7, 'Joe', 'Houpapa', NULL, 'Joe-Houpapa.png', NULL, NULL, NULL, NULL, NULL),
(8, 'Doug', 'Miller', NULL, 'doug-miller.png', NULL, NULL, NULL, NULL, NULL),
(9, 'Michael', 'Newman', NULL, 'Michael_Newman.png', NULL, NULL, NULL, NULL, NULL),
(10, 'Greg', 'Plitt', NULL, 'greg-plitt.png', NULL, NULL, NULL, NULL, NULL),
(11, 'Jenn', 'Reyes', NULL, 'Jenn-Reyes.png', NULL, NULL, NULL, NULL, NULL),
(12, 'Elena', 'Simpson', NULL, 'pilates_instructor.png', NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Struttura della tabella `livelli`
--

CREATE TABLE IF NOT EXISTS `livelli` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Nome` varchar(45) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=4 ;

--
-- Dump dei dati per la tabella `livelli`
--

INSERT INTO `livelli` (`ID`, `Nome`) VALUES
(1, 'Beginner'),
(2, 'Intermediate'),
(3, 'Advanced');

-- --------------------------------------------------------

--
-- Struttura della tabella `location`
--

CREATE TABLE IF NOT EXISTS `location` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `address` text COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=2 ;

--
-- Dump dei dati per la tabella `location`
--

INSERT INTO `location` (`ID`, `address`) VALUES
(1, 'Big Gym\npiazza Leonardo da Vinci, 32 Milano\nUnderground station Piola (M2)\n\nTel: 02-12345678\nFax: 02-12345679\nEmail: <a href="mailto:info@biggym.com">info@biggym.com</a>');

-- --------------------------------------------------------

--
-- Struttura della tabella `prizes`
--

CREATE TABLE IF NOT EXISTS `prizes` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Istruttore` int(11) NOT NULL,
  `Anno` int(11) NOT NULL,
  `Descrizione` text COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `istruttore_fk_idx` (`Istruttore`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=5 ;

--
-- Dump dei dati per la tabella `prizes`
--

INSERT INTO `prizes` (`ID`, `Istruttore`, `Anno`, `Descrizione`) VALUES
(1, 2, 1993, 'Best lifeguard of the summer, California'),
(2, 2, 1997, 'Shark killer award'),
(3, 2, 2014, 'Best instructor of the year at Big Gym'),
(4, 3, 2012, 'First woman to score highest points in Newport lifeguard training');

-- --------------------------------------------------------

--
-- Struttura della tabella `schedule`
--

CREATE TABLE IF NOT EXISTS `schedule` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Course` int(11) NOT NULL,
  `Room` text COLLATE latin1_general_ci NOT NULL,
  `Instructor` int(11) NOT NULL,
  `Day` text COLLATE latin1_general_ci NOT NULL,
  `Time` text COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=15 ;

--
-- Dump dei dati per la tabella `schedule`
--

INSERT INTO `schedule` (`ID`, `Course`, `Room`, `Instructor`, `Day`, `Time`) VALUES
(1, 15, 'Swimming Pool', 9, 'Tuesday', '11:30AM - 01:00PM'),
(2, 15, 'Swimming Pool', 9, 'Thursday', '11:30AM - 01:00PM'),
(3, 15, 'Swimming Pool', 2, 'Friday', '04:00PM - 05:30PM'),
(4, 23, 'Swimming Pool', 3, 'Monday', '03:00PM - 04:00PM'),
(5, 23, 'Swimming Pool', 3, 'Wednesday', '03:00PM - 04:00PM'),
(6, 24, 'Swimming Pool', 3, 'Monday', '04:30PM - 06:00PM'),
(7, 24, 'Swimming Pool', 3, 'Wednesday', '04:30PM - 06:00PM'),
(8, 25, 'Swimming Pool', 2, 'Tuesday', '05:00PM - 06:30PM'),
(9, 25, 'Swimming Pool', 2, 'Thursday', '05:00PM - 06:30PM'),
(10, 25, 'Swimming Pool', 2, 'Saturday', '02:30PM - 04:00PM'),
(11, 4, 'Fitness Room 2', 12, 'Monday', '04:30PM - 06:00PM'),
(12, 4, 'Fitness Room 2', 12, 'Thursday', '04:30PM - 06:00PM'),
(13, 7, 'Strength Room 1', 1, 'Monday', '05:30PM - 07:00PM'),
(14, 7, 'Strength Room 1', 1, 'Thursday', '06:30PM - 08:00PM');

-- --------------------------------------------------------

--
-- Struttura della tabella `teaches_1`
--

CREATE TABLE IF NOT EXISTS `teaches_1` (
  `Istruttore` int(11) NOT NULL,
  `Corso` int(11) NOT NULL,
  PRIMARY KEY (`Istruttore`,`Corso`),
  KEY `Corso` (`Corso`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dump dei dati per la tabella `teaches_1`
--

INSERT INTO `teaches_1` (`Istruttore`, `Corso`) VALUES
(1, 7),
(1, 8),
(1, 9),
(2, 12),
(2, 15),
(2, 25),
(3, 2),
(3, 3),
(3, 23),
(3, 24),
(9, 15),
(12, 4);

-- --------------------------------------------------------

--
-- Struttura della tabella `teaches_2`
--

CREATE TABLE IF NOT EXISTS `teaches_2` (
  `Istruttore` int(11) NOT NULL,
  `Categoria` int(11) NOT NULL,
  PRIMARY KEY (`Istruttore`,`Categoria`),
  KEY `Categoria` (`Categoria`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dump dei dati per la tabella `teaches_2`
--

INSERT INTO `teaches_2` (`Istruttore`, `Categoria`) VALUES
(1, 3),
(2, 2),
(2, 10),
(3, 2),
(3, 10),
(4, 5),
(5, 9),
(6, 9),
(7, 5),
(8, 4),
(9, 10),
(10, 5),
(11, 4);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
